# spoiler 

```
docker run --rm \
    --volume $(pwd)/flask-app:/src:z \
    --volume $(pwd)/data:/usr/share/dependency-check/data:z \
    --volume $(pwd)/odc-reports:/report:z \
    owasp/dependency-check \
    --scan /src \
    --format "ALL" \
    --enableExperimental \
    --project test \
    --out /report
```